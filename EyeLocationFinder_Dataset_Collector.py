import cv2.cv2 as cv2
import numpy as np

global mouseX,mouseY
global click_flag

mouseX = 0
mouseY = 0
click_flag = False

zone_info = [
    [(0,0),(640,360)],
    [(640,0), (1280,360)],
    [(1280,0), (1920,360)],
    [(0,360),(640,720)],
    [(640,360), (1280,720)],
    [(1280,360),(1920, 720)],
    [(0,720),(640,1080)],
    [(640,720), (1280,1080)],
    [(1280,720),(1920, 1080)]
]



def capture_mouse_click(event,x,y,flags,param):
    global mouseX,mouseY
    global click_flag
    if event == cv2.EVENT_LBUTTONDBLCLK:
        mouseX,mouseY = x,y
        click_flag = True
    

if __name__ =='__main__':
    global mouseX,mouseY,click_flag

    eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
    cap = cv2.VideoCapture(0)
    emptyImage = np.zeros((1080,1920,3),np.uint8)
    for key in zone_info:
        cv2.rectangle(emptyImage,key[0],key[1],(0,255,0),2)
    # cv2.rectangle(emptyImage,(0,720),(640,1080),(0,255,0),2)
    while True:
        ret, frame = cap.read()
        eyes = eye_cascade.detectMultiScale(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY),1.3, 10)
        
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(frame,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
        
        # cv2.imshow("Window", frame)

        cv2.namedWindow("Zone")
        cv2.setMouseCallback("Zone",capture_mouse_click)

        if click_flag == True:
            print("------------------------------- Starting here ------------------------------")
            print("Mouse Position : ",mouseX, mouseY)
            for zone in zone_info:  
                print(zone)
                print(zone[0] < (mouseX, mouseY) < zone[1])
            click_zone = []    
            click_flag = False
            print("------------------------------- Ending here ------------------------------")
            
        cv2.imshow("Zone", emptyImage)         
        k = cv2.waitKey(1)
        if k == ord('q'):
            break
